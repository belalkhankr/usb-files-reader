package simplifiedcoding.net.usbreader

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.github.mjdev.libaums.UsbMassStorageDevice
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    val fileNames = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val permissionIntent = PendingIntent.getBroadcast(this, 0, Intent(ACTION_USB_PERMISSION), 0)
        val filter = IntentFilter(ACTION_USB_PERMISSION)
        registerReceiver(usbReceiver, filter)


        val devices = UsbMassStorageDevice.getMassStorageDevices(this)

        if (devices.isEmpty()) {
            textViewDevice.text = "USB Device Not Found"
            return
        }

        val storageDevice = devices[0]
        val manager = getSystemService(Context.USB_SERVICE) as UsbManager
        manager.requestPermission(storageDevice.usbDevice, permissionIntent)

        textViewDevice.text = storageDevice.usbDevice.productName

        if (storageDevice.partitions.size <= 0) {
            Toast.makeText(this, "No partition found", Toast.LENGTH_LONG).show()
            return
        }

        val currentFs = storageDevice.partitions[0].fileSystem

        val root = currentFs.rootDirectory

        val files = root.listFiles()
        for (file in files) {
            fileNames.add(file.name)
        }
        listView.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, fileNames)
    }


    private val usbReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (ACTION_USB_PERMISSION == intent.action) {
                synchronized(this) {
                    val device: UsbDevice? = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE)
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        device?.apply {

                        }
                    } else {
                        Toast.makeText(applicationContext, "Permission Denied, Quitting Application", Toast.LENGTH_LONG)
                            .show()
                        finish()

                    }
                }
            }
        }
    }

    companion object {
        private const val ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION"
        private const val TAG = "MainActivity.kt"
    }


}
